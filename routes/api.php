<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// v1.0 of the APIs
Route::prefix('v1.0')->group(function () {

	Route::prefix('customer')->group(function () {		
		Route::post('/register', [RegistrationController::class, 'customer']);
		Route::post('/login', [LoginController::class, 'customer']);
		Route::get('/profile', [UserController::class, 'get'])->middleware('auth:sanctum');
	});
});
