<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('roles')->insert(['id'=> 1, 'name' => 'admin', 'guard_name'=>'admin', 'created_at'=> Now(), 'updated_at'=>Now()]);
		DB::table('roles')->insert(['id'=> 2, 'name' => 'customer', 'guard_name'=>'customer', 'created_at'=> Now(), 'updated_at'=>Now()]);
		DB::table('roles')->insert(['id'=> 3, 'name' => 'support', 'guard_name'=>'support', 'created_at'=> Now(), 'updated_at'=>Now()]);
		DB::table('roles')->insert(['id'=> 4, 'name' => 'supervisor', 'guard_name'=>'supervisor', 'created_at'=> Now(), 'updated_at'=>Now()]);
		DB::table('roles')->insert(['id'=> 5, 'name' => 'analyst', 'guard_name'=>'analyst', 'created_at'=> Now(), 'updated_at'=>Now()]);
		DB::table('roles')->insert(['id'=> 6, 'name' => 'auditor', 'guard_name'=>'auditor', 'created_at'=> Now(), 'updated_at'=>Now()]);
    }
}
