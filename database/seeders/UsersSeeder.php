<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Facades\DB;


class UsersSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
			'full_name' => 'Admin',
			'email' => 'admin@localhost',
			'email_verified_at' => Now(),
			'password' => '$2y$10$BwV5XdQWftEP7agI.wd93OcWDjNvtiq4Ktr322E/UvadpXJ3q03AS',
			'mobile'=>'1111111111',
			'created_at' => Now(),
			'updated_at' => Now() 
			
		]);
    }
}
