<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('customer_id');
			$table->unsignedBigInteger('support_id');
			$table->enum('status', ['open', 'progress', 'closed']);
            $table->timestamps();
			$table->foreign('customer_id')->references('id')->on('users');
			$table->foreign('support_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
