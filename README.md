## Installation Instructions
1. Have docker installed on your system
2. Build the docker image required

``` 	
sh build.sh  
```
3. Get the docker container running

```
docker-compose up -d

```

## Starting the MS
1. Create a ```.env``` 
file from the ```.env.example``` file
2. Update the .env file with the required parameters



