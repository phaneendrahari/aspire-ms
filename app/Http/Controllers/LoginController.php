<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use App\Models\User;


class LoginController extends Controller {
	
	use ApiResponser;
	
	protected function validator($req){
		$validated = Validator::make($req->all(), [
	        'email' => ['required', 'string', 'email', 'max:255'],
	        'password' => ['required', 'string', 'min:8'],
    	]);

		if($validated->fails()){
			return $validated->errors();
		}
	}
	
	public function customer(Request $req){
        
		$validation = $this->validator($req);
		if($validation){ 
			return $this->error([
				'errors' => $validation
			], 'Please check your credentials', 401);
		}

		$input = $req->all();
        if (!Auth::attempt($input)) {
            return $this->error([],'Credentials do not match', 401);
        }

        return $this->success([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ], 'Login Successful', 200);
    }

	public function profile(Request $req){
		
	}

}
