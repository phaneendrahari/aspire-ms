<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;




class RegistrationController extends Controller {

    use ApiResponser, HasRoles;
	
	protected function validator($req){
		$validated = Validator::make($req->all(), [
	        'full_name' => ['required', 'string', 'max:255'],
	        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
	        'password' => ['required', 'string', 'min:8'],
			'mobile' => ['required', 'string', 'unique:users'],
			'password_confirm' => 'required|same:password'
    	],
		[
			'email.required' => "Email address is mandatory"
		]);

		if($validated->fails()){
			return $validated->errors();
		}
	}
	
	function customer(Request $req){
		$validation = $this->validator($req);
		if($validation){ 
	
			return $this->error([
				'errors' => $validation
			], 'Registration Failed', '200');
		}
		
		$userModel = new User;
		$userModel->full_name = $req['full_name'];
		$userModel->email = $req['email'];
		$userModel->password = bcrypt($req['password']);
		$userModel->mobile = $req['mobile'];
		$userModel->email = $req['email'];
		$userModel->save();
		
		event(new Registered($userModel));
		$userModel->assignRole('customer');
		return $this->success([
            'token' => $userModel->createToken('API Token')->plainTextToken,
			'user' => $userModel
        ], "Registration Success");
		
	}

}
