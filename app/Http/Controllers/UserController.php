<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {
    
	use ApiResponser;
	
	function get(Request $req){
		return $req->user();
	}
}
